package com.example.davide.naval_battle;

import android.graphics.Color;

/**
 * Created by Davide on 30/08/2017.
 */

public class Player {

    private String playerName;
    private int playerColor;

    public int RED = 0;
    public int BLUE = 1;

    private int score = 0;


    public void setPlayerName(String Name){
        if (Name.length() > 12)
        {
            System.out.print("Name too long");
        }else
            {
            playerName = Name;
        }
    }

    public void setPlayerColor(int color){
        playerColor = color;

    }

    public void augmentScore () {
        score++;
    }


}
